/*
 * led.c
 *
 *  Created on: Jul 5, 2021
 *      Author: admin
 */

#include "led.h"

void LedInit(uint32_t pin) {
	// enable gpio D clock
	RCC->AHB1ENR |= BV(LED_EN_CLOCK);

	// set all led pins as output pins [01]
	LED_GPIO->MODER &= ~BV(pin * 2 + 1);
	LED_GPIO->MODER |= BV(pin * 2);

	// set low speed [00]
	LED_GPIO->OSPEEDR &= ~(BV(pin * 2 + 1) | BV(pin * 2));

	// configure output type to push-pull
	LED_GPIO->OTYPER &= ~BV(pin);

	// configure no pull-up & pull-down resistor
	LED_GPIO->PUPDR &= ~(BV(pin * 2 + 1) | BV(pin * 2));

}

void LedOn(uint32_t pin) {
	// write 1 to pin
	LED_GPIO->ODR |= BV(pin);
}

void LedOff(uint32_t pin) {
	// write 0 to pin
	LED_GPIO->ODR &= ~BV(pin);
}

void LedToggle(uint32_t pin) {
	// toggle the pin
	LED_GPIO->ODR ^= BV(pin);
}

void LedBlink(uint32_t pin, uint32_t ms) {
	LedOn(pin);
	DelayMs(ms);
	LedOff(pin);
}



